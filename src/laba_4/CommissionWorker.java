package laba_4;

import java.util.Calendar;

public class CommissionWorker extends Employee {
	private float commission;

	public CommissionWorker(String _name, Calendar bday) {
		super(_name, bday);
	}

	/**
	 * @param _salary Базовая зарплата
	 */
	public CommissionWorker(String _name, Calendar bday, double _salary) {
		super(_name, bday, _salary);
	}

	/**
	 * @param _commission Комиссия
	 */
	public CommissionWorker(String _name, Calendar bday, double _salary, float _commission) {
		super(_name, bday, _salary);
		if (_commission < 0) {
			throw new IllegalArgumentException("Commission cannot be less than zero");
		}
		this.commission = _commission;
	}

	@Override
	public double calculateIncome() {
		return getSalary() + getSalary() * commission / 100;
	}

	public float getCommission() {
		return commission;
	}

	public void setCommission(float commission) {
		if (commission < 0) {
			throw new IllegalArgumentException("Commission cannot be less than zero");
		}
		this.commission = commission;
	}
}
