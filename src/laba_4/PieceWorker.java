package laba_4;

import java.util.Calendar;

public class PieceWorker extends Employee {
	private long units;

	public PieceWorker(String _name, Calendar bday) {
		super(_name, bday);
	}

	/**
	 * @param _salary Плата за одно изделие
	 */
	public PieceWorker(String _name, Calendar bday, double _salary) {
		super(_name, bday, _salary);
	}

	/**
	 * @param _units Количество изготовленных изделий
	 */
	public PieceWorker(String _name, Calendar bday, double _salary, long _units) {
		super(_name, bday, _salary);
		if (_units < 0) {
			throw new IllegalArgumentException("Number of units cannot be less than zero");
		}
		this.units = _units;
	}

	@Override
	public double calculateIncome() {
		return getSalary() * units;
	}

	public long getUnitsNumber() {
		return units;
	}

	public void addUnits(long units) {
		this.units += units;
	}
}
