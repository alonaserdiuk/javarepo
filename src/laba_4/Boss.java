package laba_4;

import java.util.Calendar;

public class Boss extends Employee {
	public Boss(String _name, Calendar bday) {
		super(_name, bday);
	}

	/**
	 * @param _bossSalary Еженедельная фиксировання зарплата босса
	 */
	public Boss(String _name, Calendar bday, double _bossSalary) {
		super(_name, bday, _bossSalary);
	}

	@Override
	public double calculateIncome() {
		return getSalary();
	}
}
