package laba_4;

import java.util.Calendar;

public class HourlyWorker extends Employee {
	private long hoursWorked;
	private long extraHoursWorked;
	private double extraSalary;

	public HourlyWorker(String _name, Calendar bday) {
		super(_name, bday);
	}

	/**
	 * @param _salary Почасовая зарплата
	 */
	public HourlyWorker(String _name, Calendar bday, double _salary) {
		super(_name, bday, _salary);
	}

	/**
	 * @param hoursWorked Количество отработанных часов
	 */
	public HourlyWorker(String _name, Calendar bday, double salary, long hoursWorked) {
		this(_name, bday);
		if (salary < 0 || hoursWorked < 0) {
			throw new IllegalArgumentException("Illegal negative value passed to the constructor");
		}
		this.salary = salary;
		this.hoursWorked = hoursWorked;
	}

	/**
	 * @param extraSalary Зарплата за сверхурочную работу
	 * @param extraWorked Часы сверхурочной работы
	 */
	public HourlyWorker(String _name, Calendar bday, double salary, long hoursWorked,
	                    double extraSalary, long extraWorked) {
		this(_name, bday, salary, hoursWorked);
		if (extraSalary < 0 || extraWorked < 0) {
			throw new IllegalArgumentException("Illegal negative value passed to the constructor");
		}
		this.extraHoursWorked = extraWorked;
		this.extraSalary = extraSalary;
	}

	@Override
	public double calculateIncome() {
		return getSalary() * hoursWorked + extraSalary * extraHoursWorked;
	}

	public double getExtraSalary() {
		return extraSalary;
	}

	public void setExtraSalary(double extraSalary) {
		if (extraSalary < 0) {
			throw new IllegalArgumentException("Extra salary cannot be less than zero!");
		}
		this.extraSalary = extraSalary;
	}

	public long getHoursWorked() {
		return hoursWorked;
	}

	public void addHours(long hoursWorked) {
		this.hoursWorked += hoursWorked;
	}

	public long getExtraHoursWorked() {
		return extraHoursWorked;
	}

	public void addExtraHours(long extraHoursWorked) {
		this.extraHoursWorked += extraHoursWorked;
	}
}
