package laba_4;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EmployeeMain {
	public static void main(String[] args) {
		Calendar.Builder builder = new Calendar.Builder();
		double total = 0;
		Calendar date1 = builder.setDate(1997, 5, 20).build(),
				date2 = builder.setDate(1998, 3, 18).build(),
				date3 = builder.setDate(1998, 0, 11).build(),
				date4 = builder.setDate(1988, 11, 1).build(),
				date5 = builder.setDate(1993, 7, 28).build();
		List<Employee> employees = new ArrayList<>();
		Employee boss = new Boss("Alyona Serdiuk", date1, 456000);
		HourlyWorker worker1 = new HourlyWorker("Arina Lihota", date2, 5600, 14, 14000, 3);
		CommissionWorker comWorker = new CommissionWorker("Petr Petrov", date3, 1450.5, 3);
		Employee pWorker = new PieceWorker("Artyom Zolotaryov", date4, 500.0, 10);
		Employee hWorker = new HourlyWorker("Vermut Konjak", date5, 1100);
		worker1.setExtraSalary(10000.50);
		comWorker.setCommission(4.5f);
		employees.add(boss);
		employees.add(worker1);
		employees.add(comWorker);
		employees.add(hWorker);
		employees.add(pWorker);
		for (Employee e : employees) {
			System.out.print(e.getClass().getSimpleName() + " ");
			e.showName();
			System.out.print(" has salary " + e.getSalary() + " and earned " + e.calculateIncome());
			total += e.calculateIncome();
			System.out.println();
		}
		System.out.println("Salary to pe paid overall for work - " + total);
	}
}
