package laba_4;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public abstract class Employee {
	private static final SimpleDateFormat FORMAT = new SimpleDateFormat("dd-MM-yyyy");

	protected String name;
	protected Calendar birthday;
	protected double salary = -1;

	public Employee(String _name, Calendar bday) {
		this.name = _name;
		this.birthday = bday;
	}

	public Employee(String _name, Calendar bday, double _salary) {
		this(_name, bday);
		if (_salary < 0) {
			throw new IllegalArgumentException("Employee's salary cannot be less than zero");
		}
		this.salary = _salary;
	}

	/**
	 * @return Зарплата служащего, посчитанная на основании параметров
	 */
	public abstract double calculateIncome();

	public void showName() {
		System.out.print(name);
	}

	public String getName() {
		return name;
	}

	public Calendar getBirthday() {
		return birthday;
	}

	public double getSalary() {
		if (Math.abs(salary + 1) < 0.0001) {
			throw new IllegalStateException("Salary is not specified for this employee");
		}
		return salary;
	}

	public void setSalary(double salary) {
		if (salary < 0) {
			throw new IllegalArgumentException("Salary cannot be less than zero");
		}
		this.salary = salary;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(this.getClass().getSimpleName() + " " + name);
		str.append("\bBirthday: " + FORMAT.format(birthday) + "\n");
		if (Math.abs(salary + 1) > 0.0001) {
			str.append("Salary — " + salary + "\n");
		}
		return str.toString();
	}
}
