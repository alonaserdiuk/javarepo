package pz1;

import java.util.*;

public class PZ1 {
    private Scanner scanner = new Scanner(System.in);

    public double square(double side) {
        return side * side;
    }

    public double distance() {
        double v1, v2, S, T;
        Double temp;
        do {
            System.out.println("Input v1>0: ");
            if ((temp = getDouble()) == null) {
                System.out.println("Wrong input! Input the number > 0");
                v1 = 0;
            } else v1 = temp;
        } while (v1 <= 0);
        do {
            System.out.println("Input v2>0: ");
            if ((temp = getDouble()) == null) {
                System.out.println("Wrong input! Input the number > 0");
                v2 = 0;
            } else v2 = temp;
        } while (v2 <= 0);
        do {
            System.out.println("Input S>0: ");
            if ((temp = getDouble()) == null) {
                System.out.println("Wrong input! Input the number > 0");
                S = 0;
            } else S = temp;
        } while (S <= 0);
        do {
            System.out.println("Input T>0: ");
            if ((temp = getDouble()) == null) {
                System.out.println("Wrong input! Input the number >  0");
                T = 0;
            } else T = temp;
        } while (T <= 0);
        double commonSpeed = v1 + v2;
        return ((commonSpeed * T) < S)? S - commonSpeed * T : commonSpeed * T - S;
    }

    private Double getDouble() {
        if (!scanner.hasNextDouble()) {
            scanner.nextLine();
            return null;
        } else return scanner.nextDouble();
    }

    public void findRoots(double A, double B, double C) {
        double discriminant;
        double epsilon = 0.00001;
        if (A < epsilon) {
            throw new IllegalArgumentException("A cannot be 0 in Clazz::findRoots");
        } else if ((discriminant = B * B - 4 * A * C) < 0) {
            throw new IllegalArgumentException("Discriminant in Clazz::findRoots cannot be less than 0");
        } else {
            if (discriminant < epsilon) {
                System.out.println("x1 = x2 = " + (-B / (2 * A)));
            } else {
                System.out.println("x1 = " + ((-B - Math.sqrt(discriminant)) / (2 * A)));
                System.out.println("x2 = " + ((-B + Math.sqrt(discriminant)) / (2 * A)));
            }
        }
    }

    public void compare(int integer) {
        if (integer < 0) {
            integer -= 2;
        } else if (integer > 0) {
            integer += 1;
        } else {
            integer = 10;
        }
        System.out.println("integer = " + integer);
    }

    public void sumOf2Largest(double a, double b, double c) {
        double min = 0, sum = 0;
        min = a;
        if (b < min)
            min = b;
        if (c < min)
            min = c;
        System.out.println("Minimum = " + min);
        sum = a + b + c - min;
        System.out.println("Sum of largest two numbers is: " + sum);

    }

    public void classifyInt(int integer) {
        System.out.print(integer + " is ");
        if (integer == 0)
            System.out.println("zero");
        else if (integer < 0) {
            if (integer % 2 == 0) {
                System.out.println(" even negative");
            } else System.out.println(" odd negative");
        } else {
            if (integer % 2 == 0) {
                System.out.println(" even positive");
            } else System.out.println(" odd positive");
        }
    }

    public boolean and(int A, int B) {
        return (A > 2) && (B <= 3);
    }

    public boolean and2(int A, int B, int C) {
        return (A < B) && (B < C);
    }

    public int oneSpare(int A, int B, int C, int D) {
        if ((A == B) && (B == C))
            return 4;
        else if ((B == C) && (C == D))
            return 1;
        else if ((A == C) && (C == D))
            return 2;
        else if ((A == B) && (B == D))
            return 3;
        else throw new IllegalArgumentException("There are no three equal numbers in Clazz::oneSpare");
    }

    public void classifyMark(int K) {
        String result;
        switch (K) {
            case 1: result = "bad"; break;
            case 2: result = "not enough"; break;
            case 3: result = "just enough"; break;
            case 4: result = "good"; break;
            case 5: result = "excellent"; break;
            default: result = "error";
        }
        System.out.println("Mark is " + result);
    }

    public void getSeason(int month) {
        String result;
        switch (month) {
            case 12:
            case 1:
            case 2: result = "winter"; break;
            case 3:
            case 4:
            case 5: result = "spring"; break;
            case 6:
            case 7:
            case 8: result = "summer"; break;
            case 9:
            case 10:
            case 11: result = "autumn"; break;
            default: throw new IllegalArgumentException("month argument in Clazz::getMonth is not between 1 and 12");
        }
        System.out.println("Season is " + result);
    }

    public void ariphmetic(double A, double B, int operation) {
        double result;
        char sign;
        switch (operation) {
            case 1: result = A + B; sign = '+'; break;
            case 2: result = A - B; sign = '-'; break;
            case 3: result = A * B; sign = '*'; break;
            case 4: result = A / B; sign = '/'; break;
            default: throw new IllegalArgumentException("unknown operation in Clazz::ariphmetic");
        }
        System.out.println(A + " " + Character.toString(sign) + " " + B + " = " + result);
    }

    public void forSample(int A, int B) {
        if (A > B) {
            int temp = A;
            A = B;
            B = temp;
        }
        int counter = 0;
        for (; A <= B; ++A) {
            ++counter;
            System.out.print(A + " ");
        }
        counter -= 2;
        System.out.println("");
        System.out.println("Amount of numbers between A and B = " + counter);
    }

    public int sumBetween(int A, int B) {
        if (A > B) {
            int temp = A;
            A = B;
            B = temp;
        }
        int sum = B;
        for (;A < B; ++A) {
            sum += A;
        }
        return sum;
    }

    public int factorial(int N) {
        if (N <= 0)
            throw new IllegalArgumentException("N <= 0 in Clazz::factorial");
        int res = 1;
        for (int i = 2; i <= N; ++i) {
            res *= i;
        }
        return res;
    }

    public double findSpareSpace(double A, double B) {
        if (A < B) {
            double temp = A;
            A = B;
            B = temp;
        }
        double cur = B;
        while ((cur + B) <= A) {
            cur += B;
        }
        return A - cur;
    }

    public void findTripleCoefficient(int N) {
        int K = 1;
        while (3 * K <= N) {
            ++K;
        }
        System.out.println("3 * " + K + " > " + N);
    }

    public void printNumberReverse(int N) {
        if (N < 0)
            throw new IllegalArgumentException("N < 0 in Clazz::printNumberReverse");
        while (N != 0) {
            System.out.print((N % 10) + " ");
            N /= 10;
        }
    }
    public void printNumbersMultiple(int A, int B) {
        if (A > B) {
            int temp = A;
            A = B;
            B = temp;
        }
        int counter = 1;
        for (; A <= B; ++A) {
            for (int i = 0; i < counter; ++i) {
                System.out.print(A + " ");
            }
            ++counter;
        }
    }
}
