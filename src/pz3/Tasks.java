package pz3;

import java.util.*;

public class Tasks {
	public String concatenate(String s1, String s2) {
		if ((s1 != null && !s1.equals("")) && (s2 != null && !s2.equals(""))) {
			return s1.substring(1, s1.length()) + s2.substring(1, s2.length());
		} else {
			throw new IllegalArgumentException("Strings are inappropriate to concatenate");
		}
	}

	public String slice(String str) {
		if (str == null || str.length() < 3) {
			throw new IllegalArgumentException("String is too short to be sliced");
		}
		int length = str.length();
		return str.substring(length / 2 - 1, length / 2 + 2);
	}

	public String throwToStart(String str) {
		if (str == null || str.length() < 2) {
			throw new IllegalArgumentException("String is too short in Tasks::throwToStart");
		}
		int length = str.length();
		return str.substring(length - 2, length) + str.substring(0, length - 2);
	}

	public String doubleSymbols(String str) {
		if (str == null) {
			throw new IllegalArgumentException("Null strings not allowed!");
		}
		int counter = 0;
		char[] charAr = new char[str.length() * 2];
		for (char ch : str.toCharArray()) {
			charAr[counter++] = ch;
			charAr[counter++] = ch;
		}
		return new String(charAr);
	}

	public long bbFounder(String str) {
		if (str == null) {
			throw new IllegalArgumentException("Null strings not allowed!");
		}
		long counter = 0;
		char[] charAr = str.toCharArray();
		for (int i = 0 ; i < charAr.length - 2 ; ++i) {
			if (charAr[i] == 'b' && charAr[i + 2] == 'b')
				++counter;
		}
		return counter;
	}

	public String asteriskReplace(String str) {
		if (str == null) {
			throw new IllegalArgumentException("Null strings not allowed!");
		}
		return str.replaceAll(".?\\*+.?", "");
	}

	public long asEndingWords(String str) {
		if (str == null) {
			throw new IllegalArgumentException("Null strings not allowed!");
		}
		str = str.toLowerCase();
		String[] words = str.split("[^\\p{Alnum}а-яА-Я]+-[^\\p{Alnum}а-яА-Я]+|[\\s\\p{Punct}&&[^-]]+");
		return Arrays.stream(words)
				.filter(word -> word.charAt(word.length() - 1) == 'a'
						|| word.charAt(word.length() - 1) == 's')
				.count();
	}

	public String stringSubtraction(String basic, String toBeRemoved) {
		if (basic == null || toBeRemoved == null) {
			throw new IllegalArgumentException("Null strings not allowed!");
		}
		if (basic.length() < toBeRemoved.length() || !basic.contains(toBeRemoved)) {
			return basic;
		}
		return basic.replaceAll(toBeRemoved, "");
	}

	class TempMeasurement {
		private final List<String> months;

		{
			months = new ArrayList<>(Arrays.asList("january", "february", "march", "april",
					"may", "june", "july", "august", "september", "october", "november", "december"));
		}

		private int month;
		private List<Double> temperatures = new ArrayList<>();

		public TempMeasurement() {
			consoleInput();
		}

		public void consoleInput() {
			try (Scanner scanner = new Scanner(System.in)) {
				int monthDays;
				String line;
				System.out.print("Input month (1 or January, 2 or February and so on): ");
				line = scanner.nextLine().toLowerCase();
				while (!validateLine(line)) {
					System.out.println("Please input the correct number or literal of month.");
					line = scanner.nextLine().toLowerCase();
				}
				monthDays = getMonthDays();
				double temp;
				for (int i = 0 ; i < monthDays ; i++) {
					System.out.println("Input the average day temperature - day " + (i + 1) + ": ");
					while (true) {
						line = scanner.nextLine();
						try {
							temp = Double.parseDouble(line);
							break;
						} catch (Exception e) {
							System.out.println("Wrong input! Try again - day " + (i + 1) + ": ");
						}
					}
					temperatures.add(temp);
				}
				analyzeTemperature();
			}
		}

		private boolean validateLine(String line) {
			int month;
			if (!months.contains(line)) {
				try {
					if ((month = Integer.parseInt(line)) < 1 || month > 12) {
						return false;
					} else {
						this.month = month - 1;
					}
				} catch (Exception e) {
					return false;
				}
			} else {
				this.month = months.indexOf(line);
			}
			return true;
		}

		public int getMonthDays() {
			switch (month) {
				case 0:
				case 2:
				case 4:
				case 6:
				case 7:
				case 9:
				case 11:
					return 31;
				case 1:
					return 28;
				case 3:
				case 5:
				case 8:
				case 10:
					return 30;
				default:
					throw new IllegalStateException("Parameter month is corrupted");
			}
		}

		public void analyzeTemperature() {
			double sum = temperatures.get(0), min = temperatures.get(0), max = temperatures.get(0), cur;
			int days = temperatures.size(), dayMin = 1, dayMax = 1;
			for (int i = 1 ; i < days ; i++) {
				cur = temperatures.get(i);
				sum += cur;
				if (cur < min) {
					dayMin = i + 1;
					min = cur;
				}
				if (cur > max) {
					dayMax = i + 1;
					max = cur;
				}
			}
			String strMin = (dayMin < 10) ? "0" + dayMin : Integer.toString(dayMin),
					strMax = (dayMax < 10) ? "0" + dayMax : Integer.toString(dayMax),
					strMonth = ((month + 1) < 10) ? "0" + (month + 1) : Integer.toString(month + 1);
			sum /= (double) days;
			System.out.println("Average month temperature (" + months.get(month) + ") is " + sum + '\u00B0');
			System.out.println("Minimum temperature of month (" + strMin + "." + strMonth + ") is " + min + '\u00B0');
			System.out.println("Maximum temperature of month (" + strMax + "." + strMonth + ") is " + max + '\u00B0');
		}
	}

	public static void main(String[] args) {
		Tasks tasks = new Tasks();
		System.out.println(tasks.concatenate("aaa", "qqq"));
		System.out.println(tasks.slice("wuiotnhmklrgheu"));
		System.out.println(tasks.throwToStart("qwerty why not"));
		System.out.println(tasks.doubleSymbols("a3HJbcdefg"));
		System.out.println(tasks.asteriskReplace("**wqee*fds**123fds*"));
		System.out.println("b*b substrings - " + tasks.bbFounder("bbbbrebbbajkqurqbubbgb  bob is bab"));
		System.out.println("Words ending with 'a' or 's': " + tasks.asEndingWords(
				"owt w GRDSF Ga iewj ыав - афы fjwe if ,!1-a qewr- a-s-f e-af-ew-we- f-.!!!!!! лабs"));
		System.out.println(tasks.stringSubtraction(
				"hye what's your name my is Alyona i'm student hye what's up yo",
				"hye what's "));
		Tasks.TempMeasurement measure = tasks.new TempMeasurement();
	}
}
