package laba_3;

import java.util.*;

/** Класс "рабочий" */
public class Employee {
    /** ФИО сотрудника */
    private String name;
    /** Номер кредитной карты */
    private String creditCard;
    /** Дата рождения */
    private Calendar birthday;
    /** Телефонный номер */
    private String phoneNumber;
    /** Ежемесячный доход */
    private Double income;

    /** Разные виды конструкторов */
    public Employee(String _name, Calendar _birthday, String _phonenumber) {
        this.name = _name;
        this.birthday = _birthday;
        this.phoneNumber = _phonenumber;
    }

    public Employee(String _name, Calendar _birthday, String _phonenumber, String _creditcard) {
        this(_name, _birthday, _phonenumber);
        this.creditCard = _creditcard;
    }

    public Employee(String _name, Calendar _birthday, String _phonenumber, String _creditcard, double _income) {
        this(_name, _birthday, _phonenumber, _creditcard);
        this.income = _income;
    }

    /** Метод для вывода на экран */
    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("Name: ").append(name).append("\nBirthday: ")
                .append(Format.FORMAT.format(birthday.getTime()))
                .append(", phone number: ").append(phoneNumber);
        if (creditCard != null) {
            string.append("\nCredit card number: ").append(creditCard);
        }
        if (income != null) {
            string.append("\nMonthly income: ").append(income);
        }
        string.append("\n");
        return string.toString();
    }

    /** Сравниваем сотрудников по имени и дате рождения */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return birthday.equals(employee.getBirthday()) && name.equals(employee.getName());
    }

    /** get методы нужны для получения данных полей */
    public String getCreditCard() {
        return creditCard;
    }

    /** set методы нужны для установки данных полей */
    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public double getIncome() {
        return (income == null)? 0 : income;
    }

    public void setIncome(double income) {
        if (income < 0) {
            throw new IllegalArgumentException("income in Employee::setIncome cannot be negative!");
        }
        this.income = income;
    }

    /** Для полей name и birthday методов set не предназначено,
     * так как эти поля изменяться не могут */
    public String getName() {
        return name;
    }

    public Calendar getBirthday() {
        return birthday;
    }
}
