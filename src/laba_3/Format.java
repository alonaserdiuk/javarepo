package laba_3;

import java.text.SimpleDateFormat;

/** Форматирование вывода дат */
public class Format {
	public static final SimpleDateFormat FORMAT = new SimpleDateFormat("dd-MM-yyyy");
}
