package laba_3;

import java.util.*;

/** Класс для тестирования */
public class Main {
    public static void main(String[] args) {
        Calendar compFoundDate = new Calendar.Builder().setDate(1990, 4, 15).build();
        Company company = new Company("myComp", compFoundDate);
        company.setPresidentName("Serdyuk Alyona Yuriivna");
        System.out.println(company);
        Calendar date1 = Calendar.getInstance(), date2 = Calendar.getInstance(), date3 = Calendar.getInstance(),
        date4 = Calendar.getInstance();
        date1.set(1998, 3, 18);
        date2.set(1997, 8, 7);
        date3.set(1997, 2, 1);
        date4.set(1995, 6, 31);
        Employee Anton = new Employee("Anton", date1, "0505281376");
        Employee Arina = new Employee("Arina", date2, "0664895671",
                "1234-5678-9012-3456");
        Employee Vlad = new Employee("Vlad", date3, "0956781243",
                "1245-7891-3569-0134", 3000.0);
        Employee Max = new Employee("Max", date4, "0577209843",
                "6781-1236-4161-7435", 7060.0);
        company.setEmployees(new ArrayList<>(Arrays.asList(Max, Arina, Vlad, Anton)));
        company.sortIncome();
        company.removeEmployee(Arina);
        System.out.println(company);
        company.employeesLessThan(3500);
    }
}
