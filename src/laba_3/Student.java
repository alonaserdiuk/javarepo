package laba_3;

import java.util.Arrays;
import java.util.Calendar;

/** Класс, описывающий студента */
public class Student {
	/** ФИО студента */
	private String FIO;
	/** Студ. билет */
	private StudBilet bilet;
	/** Дата рождения */
	private Calendar birthday;
	/** Информация обуниверситете */
	private University university;
	/** Оценки студента */
	private double[] marks;

	/* Перегруженные конструкторы */
	public Student(String fio, Calendar bday, StudBilet b) {
		this.FIO = fio;
		this.birthday = bday;
		this.bilet = b;
	}

	public Student(String fio, Calendar bday, StudBilet b,  University univer, double[] _marks) {
		this(fio, bday, b);
		this.university = univer;
		this.marks = _marks;
	}

	/* Cеттеры */
	public void setUniversity(University newUniversity) {
		this.university = newUniversity;
	}

	public void setMarks(double[] newMarks) {
		this.marks = newMarks;
	}

	public void setBilet(StudBilet bilet) {
		this.bilet = bilet;
	}

	/**
	 * Вычисляет средний балл студента
	 */
	public double getAverageMark() {
		double sum = 0;
		for (int i = 0 ; i < marks.length ; ++i) {
			sum += marks[i];
		}
		return sum / marks.length;
	}

	/**
	 * Метод toString() является встроенным и предназначен для
	 * преобразования класса к текстовому виду
	 */
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Student " + FIO + "\n");
		str.append("Birthday: " + Format.FORMAT.format(birthday.getTime()) + "\n");
		str.append(bilet);
		if (university != null) {
			str.append(university);
		}
		if (marks != null && marks.length > 0) {
			str.append("Marks:" + Arrays.toString(marks));
		}
		return str.toString();
	}

	/**
	 * Главный метод программы
	 */
	public static void main(String[] args) {
		Calendar.Builder builder = new Calendar.Builder();
		Calendar cal1 = builder.setDate(2015, 8, 1).build(),
				cal2 = builder.setDate(2014, 8, 12).build(),
				cal3 = builder.setDate(1997, 5, 20).build(),
				cal4 = builder.setDate(1998, 2, 14).build();
		StudBilet bilet = new StudBilet(1234, "MT", cal1);
		StudBilet bilet2 = new StudBilet(5556, "RT", cal2);
		University univer = new University("KHNURE", "Nauki avenue", "0505281376");
		University university = new University("KHPI", "Pushkinska str 122", "0501779927");
		Student alyona = new Student("Serdyuk Alyona Yuriivna", cal3,
				bilet, univer, new double[]{90, 95, 98, 89, 80});
		Student masha = new Student("Pavlova Masha Maximovna", cal4, bilet2, university, new double[]{78, 90, 66});
		System.out.println("Average bal = " + alyona.getAverageMark());
		System.out.println(alyona);
		System.out.println(masha);
	}

	/* Методы доступа к данным */
	public StudBilet getBilet() {
		return bilet;
	}

	public double[] getMarks() {
		return marks;
	}

	public String getFIO() {
		return FIO;
	}

	public Calendar getBirthday() {
		return birthday;
	}

	public University getUniversity() {
		return university;
	}
}

