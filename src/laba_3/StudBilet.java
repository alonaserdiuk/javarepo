package laba_3;

import java.util.Calendar;

/** Класс, описывающий студенческий билет */
class StudBilet {
	/** Номер паспорта */
	private int number;
	/** Серия паспорта */
	private String seria;
	/** Дата выдачи */
	private Calendar dateOfIssue;

	/** Конструктор студ. билета */
	public StudBilet(int n, String s, Calendar date) {
		this.number = n;
		this.seria = s;
		this.dateOfIssue = date;
	}

	/** Вывод на кран */
	@Override
	public String toString() {
		return "Stud bilet #" + seria + number + ", issue date " + Format.FORMAT.format(dateOfIssue.getTime()) + "\n";
	}

	/* Геттеры */
	public int getNumber() {
		return number;
	}

	public String getSeria() {
		return seria;
	}

	public Calendar getDateOfIssue() {
		return dateOfIssue;
	}
}

