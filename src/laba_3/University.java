package laba_3;

/** Класс, описывающий университет */
class University {
	/** Название */
	private String name;
	/** Адрес */
	private String address;
	/** Контактный телефон */
	private String phone;

	/** Конструктор университета */
	public University(String _name, String _address, String _phone) {
		this.address = _address;
		this.name = _name;
		this.phone = _phone;
	}

	/** Вывод на экран */
	@Override
	public String toString() {
		return "University: name = " + name + ", address = " + address + ", phone = " + phone + "\n";
	}

	/* Геттеры */
	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getPhone() {
		return phone;
	}

	/** Если поменяют телефонный номер */
	public void setPhone(String phone) {
		this.phone = phone;
	}
}

