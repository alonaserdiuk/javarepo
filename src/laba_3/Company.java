package laba_3;

import java.util.*;

/** Класс "компания" */
public class Company {
    /** Название предприятия */
    private String name;
    /** Дата основания */
    private Calendar foundationDate;
    /** ФИО президента компании */
    private String presidentName;
    /** Список сотрудников */
    private List<Employee> employees = new ArrayList<>();

    /** Разные виды конструкторов */
    public Company(String _name, Calendar _foundationdate) {
        this.name = _name;
        this.foundationDate = _foundationdate;
    }

    public Company(String _name, Calendar _foundationdate, String _presidentname) {
        this(_name, _foundationdate);
        this.presidentName = _presidentname;
    }

    public Company(String _name, Calendar _foundationdate, String _presidentname, List<Employee> _employees) {
        this(_name, _foundationdate, _presidentname);
        this.employees = _employees;
    }

    /** Добавляет сотрудников в список. Считаем, что в компании
     * не могут быть два сотрудника с одинаковыми именами и датами рождения */
    public void addEmployee(Employee e) {
        if (employees.contains(e)) {
            throw new IllegalArgumentException("Employee " + e.getName() + " already works in the company " + name);
        }
        employees.add(e);
    }

    /** Удаляет сотрудника из списка */
    public void removeEmployee(Employee e) {
        if (!employees.contains(e)) {
            throw new IllegalArgumentException("Employee " + e.getName() + " doesn't work in the company " + name);
        }
        employees.remove(e);
    }

    /** Средний доход сотрудников предприятия */
    public double getAverageIncome() {
        double result = 0;
        for (Employee e : employees) {
            result += e.getIncome();
        }
        return result / (double)employees.size();
    }

    /** Вывод на экран сотрудников с доходом выше среднего */
    public void employeesHigherAverage() {
        double average = getAverageIncome();
        for (Employee e : employees) {
            if (e.getIncome() > average) {
                System.out.println(e);
            }
        }
    }

    /** Вывод на экран сотрудников с доходом выше заданного */
    public void employeesHigherThan(double _income) {
        for (Employee e : employees) {
            if (e.getIncome() > _income) {
                System.out.println(e);
            }
        }
    }

    /** Вывод на экран сотрудников с доходом ниже среднего */
    public void employeesLowerAverage() {
        double average = getAverageIncome();
        for (Employee e : employees) {
            if (e.getIncome() < average) {
                System.out.println(e);
            }
        }
    }

    /** Вывод на экран сотрудников с доходом нижу заданного */
    public void employeesLessThan(double _income) {
        for (Employee e : employees) {
            if (e.getIncome() < _income) {
                System.out.println(e);
            }
        }
    }

    /** Сортировка сотрудников по среднему доходу */
    public void sortIncome() {
        employees.sort((emp1, emp2) ->
            (emp1.getIncome() < emp2.getIncome())? -1 : ((emp1.getIncome() > emp2.getIncome())? 1 : 0));
    }

    /** Вывод на экран */
    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("Name: ").append(name).append("\nFoundation date: ")
                .append(Format.FORMAT.format(foundationDate.getTime()));
        if (presidentName != null) {
            string.append("\nPresident: ").append(presidentName);
        }
        if (!employees.isEmpty()) {
            string.append("\nEmployees:\n");
            employees.forEach(string::append);
        }
        string.append("\n");
        return string.toString();
    }

    /** get и set методы */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPresidentName() {
        return presidentName;
    }

    public void setPresidentName(String presidentName) {
        this.presidentName = presidentName;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    /** Для даты основания set метод не предназначен */
    public Calendar getFoundationDate() {
        return foundationDate;
    }
}
