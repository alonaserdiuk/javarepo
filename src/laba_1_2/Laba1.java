package laba_1_2;

import java.util.*;

public class Laba1 {
	private byte b;
	private short s;
	private int i;
	private long l;
	private float f;
	private double d;
	private boolean bool;
	private char ch;

	public void printClazz() {
		System.out.println("byte b = " + b);
		System.out.println("short s = " + s);
		System.out.println("int i = " + i);
		System.out.println("long l = " + l);
		System.out.println("float f = " + f);
		System.out.println("double d = " + d);
		System.out.println("boolean bool = " + bool);
		System.out.println("char ch = " + ch);
	}

	public void floatTest() {
		//float f1 = 1.; Невозможно присвоить значение типа double переменной float
		float f1 = (float)1.; //работает
		float f2 = 1;
		float f3 = 0x1;
		float f4 = 0b1;
		// float f5 = 1.0e1;
		float f5 = (float)1.0e1;
		float f6 = 01;
	}

	public void shortTest() {
		short sh;
		sh = (int)123 + (int) 1234;
		System.out.println(sh); //1357
		sh = (short)((int)12421 + 5.6);
		System.out.println(sh); //12426
		sh = (short)((int)142 + (float)5.6);
		System.out.println(sh); //147
		sh = (byte)124 + (short)1241;
		System.out.println(sh); //1365
		sh = (short)((float)5.6 + (double)9.123);
		System.out.println(sh); //14
	}

	public boolean isRightTriangle(int katet1, int katet2, int hypotenuza) {
		double presicion = 0.0001;
		return Math.abs(Math.pow(hypotenuza, 2) - Math.pow(katet1, 2) - Math.pow(katet2, 2)) < presicion;
	}

	public int sum() {
		int sum = 0;
		for (int i = 1; i <= 20; ++i) {
			sum += i;
		}
		return sum; //210
	}

	public int evenSum() {
		int sum = 0, i = 1;
		while (i <= 20) {
			if ((i % 2) == 0) {
				sum += i;
			}
			++i;
		}
		return sum; //110
	}

	public boolean isPrime(int prime) {
		int sqrt = (int)Math.sqrt(prime);
		for (int i = 2; i <= sqrt; ++i) {
			if ((prime % i) == 0) {
				return false;
			}
		}
		return true;
	}

	public int sum20Prime() {
		int sum = 0, i = 1;
		do {
			if (isPrime(i)) {
				sum += i;
			}
			i++;
		} while (i <= 20);
		return sum; //78
	}

	public void threeVars(int a, int b, int c) {
		if (((a + b) == c) || ((b + c) == a) || ((a + c) == b)) {
			System.out.println(true);
		}
	}

	public double average() {
		Random random = new Random();
		int sum = 0, size = random.nextInt(10) + 3;
		int[] arr = new int[size];
		for (int i = 0; i < size; ++i) {
			arr[i] = random.nextInt() % 301 - 150;
		}
		int b = random.nextInt(size - 1) + 1;
		int a = random.nextInt(b), counter = a;
		System.out.println(Arrays.toString(arr));
		System.out.println("a = " + a + "    b = " + b);
		do {
			sum += arr[counter];
			++counter;
		} while (counter <= b);
		return (double)sum / (double)(b - a + 1);
	}

	public void credit(double creditSum, int percent, int months) {
		if (months < 1 || percent < 0) {
			System.out.println("Wrong argument(s)!");
			return;
		}
		double perMonthSum = creditSum / (double)months;
		for (int i = 0; i < months; ++i) {
			System.out.println("Per month: sum = " + perMonthSum + ", % = "
					+ ((creditSum - perMonthSum * i) * (double)percent / 100.0) / 12.0);
			//Считаем, что ставка годовая, поэтому делим на 12
			//В каждый месяц сумма по процентам уменьшается, т.к. уменьшается еще не выплаченная сумма
		}

		System.out.println("Total % = " + (creditSum * (double)percent / 100.0 / 12.0)
				* ((double)months + 1) / 2.0);
	}

}
