package pz2;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Arrays;

/** Обработчик текста согласно заданию */
public class TextProcessor {
	/** Поле содержит текст из файла */
	private String text;

	/**
	 * @param filename Имя файла с текстом
	 */
	public TextProcessor(String filename) {
		StringBuilder str = new StringBuilder();
		String string;
		File file = new File(filename);
		if (!file.exists() || file.isDirectory()) {
			throw new IllegalArgumentException("Illegal filename. Check the source text file");
		}
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			while ((string = reader.readLine()) != null) {
				str.append(string);
			}
		} catch (IOException e) {
			throw new RuntimeException("IOException during reading the file", e);
		}
		text = str.toString();
	}

	/** Проходит циклом по всем символам и ищет цифры. Учитывает отрицательные значение */
	public long countDigitsLoop() {
		char[] charAr = this.text.toCharArray();
		if (charAr.length == 0)
			return 0;
		long counter = 0;
		if (charAr[0] >= '1' && charAr[0] <= '9') {
			counter += charAr[0] - '0';
		}
		for (int i = 1; i < charAr.length; ++i) {
			if (charAr[i] >= '1' && charAr[i] <= '9') {
				counter += (charAr[i - 1] != '-')? charAr[i] - '0' : -(charAr[i] - '0');
			}
		}
		return counter;
	}

	/** Решение с использованием классов Pattern и Matcher */
	public long countDigitsRegex() {
		long counter = 0;
		Pattern pattern = Pattern.compile("-*[1-9]");
		Matcher matcher = pattern.matcher(this.text);
		while (matcher.find()) {
			counter += Long.parseLong(matcher.group());
		}
		return counter;
	}

	/** Используем регулярные выражения и Stream API */
	public long countDigitsStreamAPI() {
		return Arrays.stream(this.text.replaceAll("[^-*1-9]", "").split("(?<=\\d)(?=(-?))"))
				.mapToInt(Integer::valueOf)
				.reduce((a, b) -> a + b).getAsInt();
	}

	/** Главный, тестирующий метод */
	public static void main(String[] args) {
		TextProcessor processor = new TextProcessor("file.txt");
		System.out.println("Loop result = " + processor.countDigitsLoop());
		System.out.println("Regex result = " + processor.countDigitsRegex());
		System.out.println("Stream result = " + processor.countDigitsStreamAPI());
	}
}